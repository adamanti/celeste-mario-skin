# Super Mario Bros. Skin
A Celeste skin mod replacing Madeline with Mario from Super Mario Bros.

### [Install the mod on Gamebanana](https://gamebanana.com/mods/251812)

---

**Features**
- Toggleable on/off
- Color palettes for different skins
- Color palettes to indicate dash count
- Toggleable outline
- Compatible with [CelesteNet](https://celestenet.0x0a.de/)

**Warning** - I literally didn't know C# when writing this my code is bad ![:cateline:](https://cdn.discordapp.com/emojis/613901947954790440.webp?size=20)
