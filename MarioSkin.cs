﻿using System;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monocle;
using Celeste;
using MonoMod;
using MonoMod.Utils;
//using Celeste.Mod.CelesteNet.Client.Entities;
using static Celeste.Mod.MarioSkin.MarioSkinStatic;

namespace Celeste.Mod.MarioSkin
{
    public static class MarioSkinStatic
    {
        public static readonly Color outlineColor = Color.Black;

        public const int marioSpriteModeValue = 110;
        public const int marioSpriteModeGhostValue = ((1 << 31) + marioSpriteModeValue);
        public const PlayerSpriteMode marioSpriteMode = (PlayerSpriteMode)marioSpriteModeValue;
        public const PlayerSpriteMode marioSpriteModeGhost = (PlayerSpriteMode)marioSpriteModeGhostValue;

        public static readonly MethodInfo lookoutLookRoutine = typeof(Lookout).GetMethod("LookRoutine", BindingFlags.Instance | BindingFlags.NonPublic);
        public const string lookoutMarioPrefix = "mario_";

        public enum ColorGradeNames
        {
            ClassicMario,
            ModernMario,
            ClassicLuigi,
            ModernLuigi,
            Shadow
        }
        public static string[] ColorGradeFolders = new string[]
        {
            "mario/classic",
            "mario/modern",
            "mario/luigi_classic",
            "mario/luigi_modern",
            "mario/shadow"
        };

        public static bool IsMarioSpriteMode(PlayerSpriteMode mode)
        {
            return marioSpriteMode <= mode && mode < (marioSpriteMode + ColorGradeFolders.Length);
        }
        public static bool IsMarioSpriteModeGhost(PlayerSpriteMode mode)
        {
            return marioSpriteModeGhost <= mode && mode < (marioSpriteModeGhost + ColorGradeFolders.Length);
        }

        public static void OutlinePlayerSprite(PlayerSprite sprite, Facings facing = Facings.Right)
        {
            DynData<PlayerSprite> spriteData = new DynData<PlayerSprite>(sprite);
            spriteData["isOutline"] = true;
            Color orig_color = sprite.Color;
            Vector2 position = sprite.Position;

            if (facing == Facings.Left) sprite.FlipX = true;
            sprite.Color = outlineColor;
            sprite.Position = position + new Vector2(0f, -1f);
            sprite.Render();
            sprite.Position = position + new Vector2(0f, 1f);
            sprite.Render();
            sprite.Position = position + new Vector2(-1f, 0f);
            sprite.Render();
            sprite.Position = position + new Vector2(1f, 0f);
            sprite.Render();

            sprite.FlipX = false;
            sprite.Color = orig_color;
            sprite.Position = position;
            spriteData.Data.Remove("isOutline");
        }

        public static void SetPlayerSpriteMode(PlayerSpriteMode? mode)
        {
            if (Engine.Scene is Level level)
            {
                Player player = level.Tracker.GetEntity<Player>();
                if (player != null)
                {
                    if (mode == null)
                    {
                        mode = player.DefaultSpriteMode;
                    }
                    if (player.Active)
                    {
                        player.ResetSpriteNextFrame((PlayerSpriteMode)mode);
                    }
                    else
                    {
                        player.ResetSprite((PlayerSpriteMode)mode);
                    }
                }
            }
        }

        public static void UpdatePlayerSpriteMode()
        {
            if (Engine.Scene is Level level)
            {
                Player player = level.Tracker.GetEntity<Player>();
                if (player != null)
                {
                    PlayerSpriteMode mode = player.Sprite.Mode;
                    if (player.Active)
                    {
                        player.ResetSpriteNextFrame(mode);
                    }
                    else
                    {
                        player.ResetSprite(mode);
                    }
                }
            }
        }

        public static string MarioColorGradeFromMode(PlayerSpriteMode mode, int? dashes = 1)
        {
            string colorGrade;
            int colorGradeIndex = (int)(mode - marioSpriteMode);
            if (mode < 0)
            {
                colorGradeIndex += (1 << 31);
            }
            string folder = ColorGradeFolders[colorGradeIndex];
            if (dashes == null)
            {
                colorGrade = folder;
            }
            else if (dashes == 0)
            {
                colorGrade = folder + "/dash0";
            }
            else if (dashes > 1)
            {
                colorGrade = folder + "/dash2";
            }
            else
            {
                colorGrade = folder + "/dash1";
            }
            return colorGrade;
        }
    }

    public class MarioSkinSettings : EverestModuleSettings
    {
        public static MarioSkinSettings Settings => MarioSkinSettings.Settings;

        private bool skinEnabled = true;
        public bool Enabled
        {
            get
            {
                return skinEnabled;
            }
            set
            {
                if (value)
                {
                    PlayerSpriteMode mode = marioSpriteMode + (int)Colors;
                    SetPlayerSpriteMode(mode);
                }
                else if (SaveData.Instance != null && SaveData.Instance.Assists.PlayAsBadeline)
                {
                    PlayerSpriteMode mode = PlayerSpriteMode.MadelineAsBadeline;
                    SetPlayerSpriteMode(mode);
                }
                else
                {
                    SetPlayerSpriteMode(null);
                }
                skinEnabled = value;
            }
        }

        private ColorGradeNames colorGrade = ColorGradeNames.ClassicMario;
        public ColorGradeNames Colors
        {
            get
            {
                return colorGrade;
            }
            set
            {
                colorGrade = value;
                if (Enabled)
                {
                    SetPlayerSpriteMode(marioSpriteMode + (int)value);
                }
            }
        }

        public bool Outline { get; set; } = false;

    }

    public class MarioSkin : EverestModule
    {
        public static MarioSkin Instance;
        public MarioSkin()
        {
            Instance = this;
        }

        public override Type SettingsType => typeof(MarioSkinSettings);
        public static MarioSkinSettings Settings => (MarioSkinSettings)Instance._Settings;


        public override void Load()
        {
            On.Celeste.PlayerSprite.ctor += PlayerSprite_ctor;
            On.Monocle.SpriteBank.CreateOn += SpriteBankCreateOn;

            On.Celeste.Player.Render += OnPlayerRender;
            On.Celeste.PlayerDeadBody.Render += OnPlayerDeadBodyRender;
            On.Celeste.PlayerSprite.Render += OnPlayerSpriteRender;

            On.Monocle.Entity.Render += OnEntityRender;
            On.Celeste.Lookout.ctor += OnLookoutCtor;
            On.Celeste.Lookout.Interact += OnLookoutInteract;
        }

        public override void Unload()
        {
            On.Celeste.PlayerSprite.ctor -= PlayerSprite_ctor;
            On.Monocle.SpriteBank.CreateOn -= SpriteBankCreateOn;

            On.Celeste.Player.Render -= OnPlayerRender;
            On.Celeste.PlayerDeadBody.Render -= OnPlayerDeadBodyRender;
            On.Celeste.PlayerSprite.Render -= OnPlayerSpriteRender;

            On.Monocle.Entity.Render -= OnEntityRender;
            On.Celeste.Lookout.ctor -= OnLookoutCtor;
            On.Celeste.Lookout.Interact -= OnLookoutInteract;
        }


        public override void Initialize()
        {
            //bool celesteNetLoaded = Everest.Loader.DependencyLoaded(new EverestModuleMetadata() { Name = "CelesteNet.Client" });
            //if (celesteNetLoaded)
            //{
            //    //OnCelesteNetClientLoaded();
            //}
        }

        /*public void OnCelesteNetClientLoaded()
        {
            Console.WriteLine(Convert.ToString(typeof(Ghost)));
        }*/


        // Load Mario's xml
        public static SpriteBank MarioSpriteBank;
        public override void LoadContent(bool firstLoad)
        {
            MarioSpriteBank = new SpriteBank(GFX.Game, "Graphics/MarioSprites.xml");
        }

        // 2 hook functions to allow Mario's sprite mode
        private void PlayerSprite_ctor(On.Celeste.PlayerSprite.orig_ctor orig, PlayerSprite self, PlayerSpriteMode mode = marioSpriteMode)
        {
            DynData<PlayerSprite> selfData = new DynData<PlayerSprite>(self);
            bool isGhost = mode < 0;
            if (!isGhost && !IsMarioSpriteMode(mode) && (Settings.Enabled && (mode == PlayerSpriteMode.Madeline || mode == PlayerSpriteMode.MadelineNoBackpack || mode == PlayerSpriteMode.MadelineAsBadeline)))
            {
                mode = marioSpriteMode + (int)Settings.Colors;
            }
            else if (isGhost)
            {
                selfData["isGhost"] = true;
            }
            orig(self, mode);
            if (IsMarioSpriteMode(mode) || IsMarioSpriteModeGhost(mode))
            {
                string id = "mario";
                selfData["spriteName"] = id;
                MarioSpriteBank.CreateOn(self, id);
            }
        }
        private Sprite SpriteBankCreateOn(On.Monocle.SpriteBank.orig_CreateOn orig, SpriteBank self, Sprite sprite, string id)
        {
            if (sprite is PlayerSprite && id == "")
            {
                return null;
            }
            return orig(self, sprite, id);
        }


        private void OnPlayerRender(On.Celeste.Player.orig_Render orig, Player self)
        {
            if (IsMarioSpriteMode(self.Sprite.Mode))
            {
                new DynData<PlayerSprite>(self.Sprite)["marioColorGrade"] = MarioColorGradeFromMode(self.Sprite.Mode, self.Dashes);
                orig(self);
            }
            else
            {
                orig(self);
            }
        }

        private void OnPlayerDeadBodyRender(On.Celeste.PlayerDeadBody.orig_Render orig, PlayerDeadBody self)
        {
            PlayerSprite sprite = new DynData<PlayerDeadBody>(self).Get<PlayerSprite>("sprite");
            if (IsMarioSpriteMode(sprite.Mode) || IsMarioSpriteModeGhost(sprite.Mode))
            {
                new DynData<PlayerSprite>(sprite)["marioColorGrade"] = MarioColorGradeFromMode(sprite.Mode);
                orig(self);
            }
            else
            {
                orig(self);
            }
        }

        private void OnPlayerSpriteRender(On.Celeste.PlayerSprite.orig_Render orig, PlayerSprite self)
        {
            DynData<PlayerSprite> selfData = new DynData<PlayerSprite>(self);
            if (selfData.Get<bool?>("isOutline") == null && (IsMarioSpriteMode(self.Mode) || IsMarioSpriteModeGhost(self.Mode)))
            {
                if (MarioSkin.Settings.Outline)
                {
                    MarioSkinStatic.OutlinePlayerSprite(self, Facings.Right);
                }

                if (selfData.Get<bool?>("isGhost") == true)
                {
                    selfData["marioColorGrade"] = MarioColorGradeFromMode(self.Mode);
                }

                string colorGrade = selfData.Get<string>("marioColorGrade");

                if (colorGrade != null)
                {
                    Effect colorGradeEffect = GFX.FxColorGrading;
                    colorGradeEffect.CurrentTechnique = colorGradeEffect.Techniques["ColorGradeSingle"];
                    Engine.Graphics.GraphicsDevice.Textures[1] = GFX.ColorGrades[colorGrade].Texture.Texture_Safe;

                    // let's try to colorgrade Maddy. :maddyS:
                    Scene scene = self.Scene ?? Engine.Scene;
                    Draw.SpriteBatch.End();
                    Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.None, RasterizerState.CullNone, colorGradeEffect, (scene as Level).GameplayRenderer.Camera.Matrix);
                    orig(self);
                    Draw.SpriteBatch.End();
                    Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.None, RasterizerState.CullNone, null, (scene as Level).GameplayRenderer.Camera.Matrix);
                }
                else
                {
                    orig(self);
                }
            }
            else
            {
                orig(self);
            }
        }


        private void OnEntityRender(On.Monocle.Entity.orig_Render orig, Entity self)
        {
            if (self is Lookout)
            {
                DynData<Lookout> lookoutData = new DynData<Lookout>((Lookout)self);
                string colorGrade = lookoutData.Get<string>("marioColorGrade");
                if (lookoutData.Get<bool>("interacting") && colorGrade != "")
                {
                    Effect colorGradeEffect = GFX.FxColorGrading;
                    colorGradeEffect.CurrentTechnique = colorGradeEffect.Techniques["ColorGradeSingle"];
                    Engine.Graphics.GraphicsDevice.Textures[1] = GFX.ColorGrades[colorGrade].Texture.Texture_Safe;

                    Scene scene = self.Scene ?? Engine.Scene;
                    Draw.SpriteBatch.End();
                    Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.None, RasterizerState.CullNone, colorGradeEffect, (scene as Level).GameplayRenderer.Camera.Matrix);
                    orig(self);
                    Draw.SpriteBatch.End();
                    Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.None, RasterizerState.CullNone, null, (scene as Level).GameplayRenderer.Camera.Matrix);
                }
                else
                {
                    orig(self);
                }
            }
            else
            {
                orig(self);
            }
        }

        private void OnLookoutCtor(On.Celeste.Lookout.orig_ctor orig, Lookout self, EntityData data, Vector2 offset)
        {
            orig(self, data, offset);
            DynData<Lookout> selfData = new DynData<Lookout>(self);
            MarioSpriteBank.CreateOn((Sprite)(selfData["sprite"]), "lookout");
            selfData["marioColorGrade"] = "";
        }

        private void OnLookoutInteract(On.Celeste.Lookout.orig_Interact orig, Lookout self, Player player)
        {
            orig(self, player);
            DynData<Lookout> selfData = new DynData<Lookout>(self);
            if (IsMarioSpriteMode(player.Sprite.Mode))
            {
                selfData["animPrefix"] = lookoutMarioPrefix;
                selfData["marioColorGrade"] = MarioColorGradeFromMode(player.Sprite.Mode, null) + "/lookout"; // Could remove `null` and `+ "/lookout"` if I made the lookout an overlayed sprite in the future
            }
            else
            {
                selfData["marioColorGrade"] = "";
            }
        }
    }

}
